defmodule Maumau do
  def play(file) do
    file
    |> Maumau.Reader.entry()
    |> Enum.map(&Task.async(Maumau.Game, :start_game, [&1]))
    |> Enum.map(&Task.await(&1, :infinity))
    |> Enum.map(&print_result/1)
  end

  defp print_result({game, %{players: [player]}}) do
    "Vencedor da partida #{game}: Jogador #{player[:player]}"
  end
end
