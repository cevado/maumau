defmodule Maumau.Card do
  def action(nil), do: :buy
  def action({"G", _}), do: {:buy, 2}
  def action({"I", _}), do: {:buy, 3}
  def action({"A", _}), do: :pass
  def action({"L", _}), do: :reverse
  def action({_, _}), do: :neutral

  def discard(cards, card), do: cards -- [card]

  def sort(cards), do: Enum.sort(cards, &compare/2)

  def points({<<x>>, _}), do: x - 64

  def sum_points(%{hand: cards}) do
    cards
    |> Enum.map(&points/1)
    |> Enum.sum()
  end

  def compare({value, naipe1}, {value, naipe2}), do: naipe1 < naipe2
  def compare({value1, _naipe1}, {value2, _naipe2}), do: value1 > value2

  def choose({value, _naipe1} = card, {value, _naipe2}), do: card
  def choose({_value1, naipe} = card, {_value2, naipe}), do: card
  def choose(_card1, _card2), do: false

  def play(cards, nil) do
    cards |> sort() |> List.first()
  end

  def play(cards, card) do
    Enum.find_value(cards, nil, &choose(&1, card))
  end
end
