defmodule Maumau.Round do
  alias Maumau.Turn

  def play(%{finished_round: true} = state), do: state

  def play(state) do
    state
    |> discard_to_deck()
    |> finished()
    |> Turn.play()
    |> play()
  end

  def discard_to_deck(%{deck: [], discard: [top | discard]} = state) do
    %{state | deck: Enum.reverse(discard), discard: [top]}
  end

  def discard_to_deck(state), do: state

  defp finished(%{players: players} = state) do
    index =
      players
      |> Enum.with_index()
      |> Enum.find({nil, nil}, &Enum.empty?(elem(&1, 0)[:hand]))
      |> elem(1)

    %{state | finished_round: index == state[:player_index]}
  end
end
