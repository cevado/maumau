defmodule Maumau.Player do
  alias Maumau.{Card, Round}

  def current(state), do: Enum.at(state[:players], state[:player_index])

  def discard(state, index, card) do
    state
    |> Map.get(:players)
    |> Enum.at(index)
    |> Map.get(:hand)
    |> Card.discard(card)
    |> then(&update(state, index, &1))
  end

  def update(state, index, cards) do
    put_in(state, [:players, Access.at(index), :hand], Card.sort(cards))
  end

  def buy_cards(state, amount) when amount <= 0, do: state

  def buy_cards(state, amount) do
    player = next(state)
    hand = get_in(state, [:players, Access.at(player), :hand])
    {cards, deck} = Enum.split(state[:deck], amount)

    state
    |> Map.put(:deck, deck)
    |> update(player, cards ++ hand)
    |> Round.discard_to_deck()
    |> buy_cards(amount - length(cards))
  end

  def next(state), do: next(state[:player_index], state[:direction], length(state[:players]) - 1)

  defp next(current, 1, current), do: 0
  defp next(0, -1, max), do: max
  defp next(current, direction, _max), do: current + direction
end
