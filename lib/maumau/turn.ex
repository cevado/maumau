defmodule Maumau.Turn do
  alias Maumau.{Card, Player}

  def play(state) do
    player = Player.current(state)
    top = top(state)
    card = Card.play(player[:hand], top)

    card
    |> Card.action()
    |> apply_action(card, player, state)
    |> next
  end

  def apply_action(:buy, nil, player, %{deck: [card | deck]} = state) do
    state = Map.put(state, :deck, deck)

    case Card.play([card], top(state)) do
      nil -> Player.update(state, state[:player_index], [card | player[:hand]])
      _card -> Map.put(state, :discard, [card | state[:discard]])
    end
  end

  def apply_action({:buy, amount}, card, player, state) do
    state = Player.buy_cards(state, amount)

    :neutral
    |> apply_action(card, player, state)
  end

  def apply_action(:reverse, card, player, state) do
    :neutral
    |> apply_action(card, player, state)
    |> Map.put(:direction, state[:direction] * -1)
  end

  def apply_action(:pass, card, player, state) do
    :neutral
    |> apply_action(card, player, state)
    |> next
  end

  def apply_action(:neutral, card, _player, state) do
    state
    |> Player.discard(state[:player_index], card)
    |> Map.put(:discard, [card | state[:discard]])
  end

  def next(state) do
    state
    |> Player.next()
    |> then(&Map.put(state, :player_index, &1))
  end

  defp top(state), do: Enum.at(state[:discard], 0)
end
