defmodule Maumau.Reader do
  def entry(file) do
    file
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.trim/1)
    |> prepare_games()
  end

  defp prepare_games([_games | games]) do
    build_game(games, [], 1)
  end

  defp build_game([], content, _game), do: Enum.reverse(content)

  defp build_game([players | rest], content, game) do
    players = String.to_integer(players)
    {decks, rest} = Enum.split(rest, players - 1)
    decks = Enum.map(decks, &build_deck/1)
    build_game(rest, [{players, decks, game} | content], game + 1)
  end

  defp build_deck(deck) do
    deck
    |> String.split()
    |> Enum.map(&build_card/1)
  end

  def build_card(card) do
    card
    |> String.graphemes()
    |> List.to_tuple()
  end
end
