defmodule Maumau.Game do
  alias Maumau.{Round, Card, Player}

  def start_game({players, decks, game}) do
    state =
      players
      |> build_game()
      |> play(decks)

    {game, state}
  end

  def play(state, []), do: state

  def play(state, [deck | decks]) do
    state
    |> Map.put(:deck, deck)
    |> handle_cards()
    |> Round.play()
    |> remove_loser()
    |> clean_table()
    |> play(decks)
  end

  def build_game(players) do
    players = Enum.map(1..players, &build_player/1)

    %{
      deck: [],
      discard: [],
      direction: 1,
      player_index: 0,
      players: players,
      finished_round: false
    }
  end

  defp build_player(player) do
    %{player: player, hand: []}
  end

  def handle_cards(%{players: players, deck: deck} = state) do
    {players, deck} = Enum.reduce(players, {[], deck}, &handle_card/2)

    %{state | deck: Enum.reverse(deck), players: Enum.reverse(players)}
  end

  def handle_card(player, {players, deck}) do
    {cards, deck} = Enum.split(deck, 5)
    player = %{player | hand: Maumau.Card.sort(cards)}
    {[player | players], deck}
  end

  defp remove_loser(state) do
    state[:players]
    |> Enum.map(&Card.sum_points/1)
    |> Enum.with_index()
    |> Enum.max_by(&elem(&1, 0))
    |> elem(1)
    |> then(&List.delete_at(state[:players], &1))
    |> then(&Map.put(state, :players, &1))
    |> debug
  end

  def clean_table(state) do
    state
    |> clean_hands()
    |> Map.put(:deck, [])
    |> Map.put(:discard, [])
  end

  defp clean_hands(state) do
    0..(length(state[:players]) - 1)
    |> Enum.reduce(state, &Player.update(&2, &1, []))
  end

  defp debug(%{players: players} = state) do
    state
  end
end
